package fr.fr2i;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

/**
 * Created by frederic on 04/05/15.
 */
public class FizzBuzzTest {

    private FizzBuzz fizzBuzz;


    @Before
    public void avantChaqueTest()
    {
        fizzBuzz = new FizzBuzz();
    }

    @Test
    public void retourneResultNombre()
    {
        Assert.assertEquals("1", fizzBuzz.result(1));
    }

    @Test
    public void retourneResultFizz()
    {
        Assert.assertEquals("fizz", fizzBuzz.result(3));
    }

    @Test
    public void retourneResultBuzz()
    {
        Assert.assertEquals("buzz", fizzBuzz.result(5));
    }

    @Test
    public void retourneResultFizzBuzz()
    {
        Assert.assertEquals("fizzbuzz", fizzBuzz.result(15));
    }







}
